"""
EXCEL data:
file: TABELA-GERAL-MAPBIOMAS-COL8.0-TERRAS_INDIGENAS.xlsx
sheets:
- COBERTURA_COL8.0

file: JME_Country_Estimates_May_2023.xlsx
sheets:
- Stunting Proportion (Model)

file: TABELA-MINERACAO-MAPBIOMAS-COL8.0.xlsx
sheets:
- CITY_STATE_BIOME
"""

import pandas as pd
from tabulate import tabulate


def load_excel_column(file_path, sheet_name):
	"""
	Loads specific columns from an Excel file sheet.

	Parameters:
	file_path (str): Path to the Excel file.
	sheet_name (str): Name of the sheet to load.
	columns (list): List of column names to load.

	Returns:
	pd.DataFrame: DataFrame containing the specified columns.
	"""
	try:
		return pd.read_excel(file_path, sheet_name=sheet_name, engine="openpyxl")
	except Exception as e:
		print(f"An error occurred: {e}")
		return None


df_indigenous = load_excel_column("./ressources/TABELA-GERAL-MAPBIOMAS-COL8.0-TERRAS_INDIGENAS.xlsx", "COBERTURA_COL8.0")
print(tabulate(df_indigenous.iloc[:10], headers="keys", tablefmt="pretty"))

df_malnourishment = load_excel_column("./ressources/JME_Country_Estimates_May_2023.xlsx", "Stunting Proportion (Model)")
print(tabulate(df_malnourishment.iloc[:10], headers="keys", tablefmt="pretty"))

df_mining = load_excel_column("./ressources/TABELA-MINERACAO-MAPBIOMAS-COL8.0.xlsx", "CITY_STATE_BIOME")
print(tabulate(df_mining.iloc[:10], headers="keys", tablefmt="pretty"))


def read_tfw(file_path):
	"""
	Reads a .tfw file and returns its content.

	Parameters:
	file_path (str): Path to the .tfw file.

	Returns:
	list: List containing the six values from the .tfw file.
	"""
	try:
		with open(file_path, "r") as file:
			content = file.readlines()
			# Strip any whitespace characters like \n at the end of each line
			content = [float(line.strip()) for line in content]
		return content
	except Exception as e:
		print(f"An error occurred: {e}")
		return None


# Example usage
file_path_tfw = "./ressources/co-ve2427/co-ve2427m.tfw"
tfw_content = read_tfw(file_path_tfw)
print(tfw_content)
