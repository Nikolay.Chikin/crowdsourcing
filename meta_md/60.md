## Title

Immunization Campaigns Impacted due to COVID-19

## Notes

Immunization campaigns impacted due to COVID-19

## Dataset Source

Multiple sources

## Organization

World Health Organization

## Dataset Date

[2020-12-14T00:00:00 TO 2023-11-09T23:59:59]

## Last Modified

2023-10-29T01:01:50.117775

## Data Update Frequency

Live

## Methodology

Registry

## Tags

covid-19, hxl, vaccination-immunization