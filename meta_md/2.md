## Title

Brazil - Requirements and Funding Data

## Notes

FTS publishes data on humanitarian funding flows as reported by donors and recipient organizations. It presents all humanitarian funding to a country and funding that is specifically reported or that can be specifically mapped against funding requirements stated in humanitarian response plans. The data comes from OCHA's Financial Tracking Service, is encoded as utf-8 and the second row of the CSV contains HXL tags.

## Dataset Source

OCHA Financial Tracking Service

## Organization

OCHA Financial Tracking System (FTS)

## Dataset Date

[2024-05-10T00:00:00 TO 2024-05-10T23:59:59]

## Last Modified

2024-05-10T02:10:33.357117

## Data Update Frequency

Every day

## Methodology

Other

## Methodology Other

FTS is open to all government and private donors, funds, recipient agencies and implementing organizations wishing to report financial pledges and contributions for humanitarian action. FTS team curates the reported data by manually reviewing, validating and reconciling incoming information and updating FTS data records on an ongoing basis.

## Caveats

Please see [About FTS](https://fts.unocha.org/content/about-fts-what-fts)

## Tags

covid-19, funding, hxl