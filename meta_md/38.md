## Title

Brazil: WOF Administrative Subdivisions and Human Settlements

## Notes

This dataset contains administrative polygons grouped by country (admin-0) with the following subdivisions according to Who's On First placetypes: - macroregion (admin-1 including region) - region (admin-2 including state, province, department, governorate) - macrocounty (admin-3 including arrondissement) - county (admin-4 including prefecture, sub-prefecture, regency, canton, commune) - localadmin (admin-5 including municipality, local government area, unitary authority, commune, suburb)   The dataset also contains human settlement points and polygons for: - localities (city, town, and village) - neighbourhoods (borough, macrohood, neighbourhood, microhood)   The dataset covers activities carried out by Who's On First (WOF) since 2015. Global administrative boundaries and human settlements are aggregated and standardized from hundreds of sources and available with an open CC-BY license. Who's On First data is updated on an as-need basis for individual places with annual sprints focused on improving specific countries or placetypes. Please refer to the README.md file for complete data source metadata. Refer to our blog post for explanation of field names.   Data corrections can be proposed using Write Field, an web app for making quick data edits. You’ll need a Github.com account to login and propose edits, which are then reviewed by the Who's On First community using the Github pull request process. Approved changes are available for download within 24-hours. Please contact WOF admin about bulk edits.

## Dataset Source

Multiple sources

## Organization

Who's On First

## Dataset Date

[2015-08-18T00:00:00 TO 2024-03-05T23:59:59]

## Last Modified

2024-04-12T13:52:41.609766

## Data Update Frequency

Every year

## Methodology

Other

## Methodology Other

The Who's On First dataset is both an original work and a modification of existing [open data sources](https://whosonfirst.org/docs/licenses/). Some of those open data projects do require attribution. We detail our more than 300 authoritative sources, including national mapping agencies and census ministries, and their specific license, usage, and vintage metadata in the full [sources list](https://whosonfirst.org/docs/sources/list/).

## Caveats

Who's On First data is updated on an as needed basis. After a database update, the new data will be available for download within 24-hours. Not all countries include features and/or polygons for the full range of administrative levels and some others may be out-of-date or have poor spatial resolution. Some populated place human settlements may be missing because of older, incomplete, or inaccurate source material and most are represented as a point geometry.

## Tags

administrative boundaries-divisions, geodata, populated places-settlements, population