## Title

Refugee and Migrant Needs Analysis (RMNA) - People in Need 2023

## Notes

The Regional Inter-Agency Coordination Platform for Refugees and Migrants from Venezuela (R4V) issued the Refugee and Migrant Needs Analysis (RMNA) for 2023 to provide insights into the situation and varied challenges that refugees and migrants – both in-destination and in-transit – as well as their affected host communities face. The RMNA draws upon an extensive range of sources, with special emphasis on the inter-agency joint needs assessments (JNA) conducted by R4V partners in various countries.

## Dataset Source

Inter-Agency Coordination Platform for Refugees and Migrants from Venezuela

## Organization

Inter-Agency Coordination Platform for Refugees and Migrants from Venezuela

## Dataset Date

[2023-05-01T00:00:00 TO 2023-07-31T23:59:59]

## Last Modified

2023-09-13T15:42:20.919253

## Data Update Frequency

Every year

## Methodology

Other

## Methodology Other

The RMNA draws upon an extensive range of sources, with special emphasis on the inter-agency joint needs assessments (JNA) conducted by R4V partners in various countries. Of particular importance are the primary data collection exercises, led by R4V, which comprehensively assess various sectoral needs.

## Tags

needs assessment, people in need-pin