## Title

Brazil - Conflict Events

## Notes

A weekly dataset providing the total number of reported political violence, civilian-targeting, and demonstration events in Brazil. Note: These are aggregated data files organized by country-year and country-month. To access full event data, please register to use the Data Export Tool and API on the ACLED website.

## Dataset Source

Armed Conflict Location & Event Data Project (ACLED)

## Organization

Armed Conflict Location & Event Data Project (ACLED)

## Dataset Date

[2018-01-01T00:00:00 TO 2024-05-03T23:59:59]

## Last Modified

2024-05-08T17:47:14.490474

## Data Update Frequency

Every week

## Methodology

Other

## Methodology Other

Please review the [ACLED Codebook](https://acleddata.com/download/2827/) for more information about event definitions, sub-event definitions, and coding methodology. For more information about our fatality and sourcing methodology, please review our [Fatality FAQs](https://acleddata.com/acleddatanew/wp-content/uploads/dlm_uploads/2020/02/FAQs_-ACLED-Fatality-Methodology_2020.pdf) and [Sourcing FAQs](https://acleddata.com/acleddatanew/wp-content/uploads/dlm_uploads/2020/02/FAQs_ACLED-Sourcing-Methodology.pdf). Time period coverage varies by country and region, so please consult the ACLED [Coverage List](https://acleddata.com/download/4404/). For more methodology resources, click [here](https://acleddata.com/resources/general-guides/).

## Tags

conflict-violence