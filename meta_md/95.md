## Title

Brazil: Count of Threat in Belo Horizonte

## Notes

Places where threat have been carried out within Belo Horizonte,  period  2017-2021

## Dataset Source

Instituto de Geografia – USFQ and U.S Department of State

## Organization

Hub Latin America

## Dataset Date

[2017-01-01T00:00:00 TO 2021-06-30T23:59:59]

## Last Modified

2021-10-10T19:00:11.419331

## Data Update Frequency

Every year

## Methodology

Registry

## Caveats

Created by UFMG, from Police Database

## Tags

gender-based violence-gbv