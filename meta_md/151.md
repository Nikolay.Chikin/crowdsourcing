## Title

HOTOSM Brazil (North) Airports (OpenStreetMap Export)

## Notes

OpenStreetMap exports for use in GIS applications. This theme includes all OpenStreetMap features in this area matching: aeroway IS NOT NULL OR building = 'aerodrome' OR emergency:helipad IS NOT NULL OR emergency = 'landing_site' Features may have these attributes:  capacity:persons name emergency:helipad addr:full emergency source operator:type addr:city building aeroway  This dataset is one of many OpenStreetMap exports on HDX. See the Humanitarian OpenStreetMap Team website for more information.

## Dataset Source

OpenStreetMap contributors

## Organization

Humanitarian OpenStreetMap Team (HOT)

## Dataset Date

[2023-03-07T00:00:00 TO 2023-03-07T23:59:59]

## Last Modified

2020-06-10T22:01:52.425613

## Data Update Frequency

Every month

## Methodology

Other

## Methodology Other

Volunteered geographic information

## Caveats

OpenStreetMap data is crowd sourced and cannot be considered to be exhaustive

## Tags

aviation, facilities-infrastructure, geodata, transportation