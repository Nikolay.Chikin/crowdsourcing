## Title

Brazil Health Facilities (OpenStreetMap Export)

## Notes

OpenStreetMap exports for use in GIS applications. This theme includes all OpenStreetMap features in this area matching ( Learn what tags means here ) : tags['healthcare'] IS NOT NULL OR tags['amenity'] IN ('doctors', 'dentist', 'clinic', 'hospital', 'pharmacy') Features may have these attributes:  name name:en amenity building healthcare healthcare:speciality operator:type capacity:persons addr:full addr:city source name:pt  This dataset is one of many OpenStreetMap exports on HDX. See the Humanitarian OpenStreetMap Team website for more information.

## Dataset Source

OpenStreetMap contributors

## Organization

Humanitarian OpenStreetMap Team (HOT)

## Dataset Date

[2024-04-05T00:00:00 TO 2024-04-05T23:59:59]

## Last Modified

2024-04-05T15:57:14.288087

## Data Update Frequency

Every month

## Methodology

Other

## Methodology Other

Volunteered geographic information

## Caveats

OpenStreetMap data is crowd sourced and cannot be considered to be exhaustive

## Tags

geodata, health, health facilities