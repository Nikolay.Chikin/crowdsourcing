## Title

Survey on Gender Equality At Home

## Notes

This data contains aggregated weighted statistics at the regional level by gender for the 2020 Survey on Gender Equality At Home as well as the country and regional level for the 2021 wave. The Survey on Gender Equality at Home generates a global snapshot of women and men’s access to resources, their time spent on unpaid care work, and their attitudes about equality. Researchers and nonprofits interested in access to survey microdata can apply at: https://dataforgood.facebook.com/dfg/tools/survey-on-gender-equality-at-home

## Dataset Source

Facebook

## Organization

Data for Good at Meta

## Dataset Date

[2020-09-21T00:00:00 TO 2020-09-21T23:59:59]

## Last Modified

2022-01-14T18:47:09.262646

## Data Update Frequency

As needed

## Methodology

Sample Survey

## Caveats

Some countries were excluded due to data collection restrictions (e.g. Syria, Iran, Cuba, Crimea, North Korea, China, Venezuela).

## Tags

covid-19, education, gender, health