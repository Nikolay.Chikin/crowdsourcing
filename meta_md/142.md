## Title

HOTOSM Brazil (South1) Railways (OpenStreetMap Export)

## Notes

OpenStreetMap exports for use in GIS applications. This theme includes all OpenStreetMap features in this area matching: railway IN ('rail','subway','station') Features may have these attributes:  railway ele addr:city operator:type name layer addr:full source  This dataset is one of many OpenStreetMap exports on HDX. See the Humanitarian OpenStreetMap Team website for more information.

## Dataset Source

OpenStreetMap contributors

## Organization

Humanitarian OpenStreetMap Team (HOT)

## Dataset Date

[2021-02-19T00:00:00 TO 2021-02-19T23:59:59]

## Last Modified

2020-06-11T22:02:38.083962

## Data Update Frequency

Every month

## Methodology

Other

## Methodology Other

Volunteered geographic information

## Caveats

OpenStreetMap data is crowd sourced and cannot be considered to be exhaustive

## Tags

facilities-infrastructure, geodata, railways, transportation