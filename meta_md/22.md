## Title

Brazil - Poverty

## Notes

Contains data from the World Bank's data portal. There is also a consolidated country dataset on HDX. For countries with an active poverty monitoring program, the World Bank—in collaboration with national institutions, other development agencies, and civil society—regularly conducts analytical work to assess the extent and causes of poverty and inequality, examine the impact of growth and public policy, and review household survey data and measurement methods.  Data here includes poverty and inequality measures generated from analytical reports, from national poverty monitoring programs, and from the World Bank’s Development Research Group which has been producing internationally comparable and global poverty estimates and lines since 1990.

## Dataset Source

World Bank

## Organization

World Bank Group

## Dataset Date

[1981-01-01T00:00:00 TO 2022-12-31T23:59:59]

## Last Modified

2024-04-27T13:51:34.552454

## Data Update Frequency

Every month

## Methodology

Registry

## Caveats



## Tags

hxl, indicators, poverty