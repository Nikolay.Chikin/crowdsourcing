## Title

Brazil - External Debt

## Notes

Contains data from the World Bank's data portal. There is also a consolidated country dataset on HDX. Debt statistics provide a detailed picture of debt stocks and flows of developing countries. Data presented as part of the Quarterly External Debt Statistics takes a closer look at the external debt of high-income countries and emerging markets to enable a more complete understanding of global financial flows. The Quarterly Public Sector Debt database provides further data on public sector valuation methods, debt instruments, and clearly defined tiers of debt for central, state and local government, as well as extra-budgetary agencies and funds. Data are gathered from national statistical organizations and central banks as well as by various major multilateral institutions and World Bank staff.

## Dataset Source

World Bank

## Organization

World Bank Group

## Dataset Date

[1960-01-01T00:00:00 TO 2022-12-31T23:59:59]

## Last Modified

2024-04-27T13:52:18.412708

## Data Update Frequency

Every month

## Methodology

Registry

## Caveats



## Tags

economics, hxl, indicators