## Title

RMNA 2022

## Notes

Full dataset of the 2022 Refugee and Migrant Needs Analysis (RMNA), including total population and people in need estimates disaggregated by age and gender, and Admin-1 level. More info on the RMNA microsite: https://rmrp.r4v.info/rmna/

## Dataset Source

Inter-Agency Coordination Platform for Refugees and Migrants from Venezuela

## Organization

Inter-Agency Coordination Platform for Refugees and Migrants from Venezuela

## Dataset Date

[2022-10-01T00:00:00 TO *]

## Last Modified

2023-01-09T19:11:10.378171

## Data Update Frequency

Every year

## Methodology

Sample Survey

## Tags

demographics, needs assessment, people in need-pin, refugees