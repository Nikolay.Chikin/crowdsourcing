## Title

Aid Worker KIKA (Killed, Injured, Kidnapped or Arrested) Data

## Notes

This dataset contains agency- and publicly-reported data for events in which an aid worker was killed, injured, kidnapped, or arrested (KIKA). Categorized by country. Please get in touch if you are interested in curated datasets: info@insecurityinsight.org

## Dataset Source

Insecurity Insight

## Organization

Insecurity Insight

## Dataset Date

[2020-01-01T00:00:00 TO 2024-03-28T23:59:59]

## Last Modified

2024-04-30T16:25:35.833554

## Data Update Frequency

Every month

## Methodology

Other

## Methodology Other

Systematically collected from open source, public reports as well as verified submissions from our partner agencies.

## Caveats

Not representative or a comprehensive compilation of all events in which an aid worker was killed, kidnapped, or arrested. 
Key definitions
Aid worker: An individual employed by or attached to a humanitarian, UN, international, national, or government aid agency.
Killed: Refers to a staff member being killed. Aid worker(s) killed while in captivity are coded as ‘kidnapped’.
Kidnapped: Refers to a staff member being kidnapped, missing or taken hostage.
Arrested: Refers to a staff member being arrested, charged, detained, fined or imprisoned.  
Data collection is ongoing and data may change as more information is made available.

## Tags

aid worker security, aid workers, conflict-violence, hxl, indicators