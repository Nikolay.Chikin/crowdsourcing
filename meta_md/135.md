## Title

HOTOSM Brazil (South2) Roads (OpenStreetMap Export)

## Notes

OpenStreetMap exports for use in GIS applications. This theme includes all OpenStreetMap features in this area matching: highway IS NOT NULL Features may have these attributes:  layer highway oneway surface width smoothness source name bridge lanes  This dataset is one of many OpenStreetMap exports on HDX. See the Humanitarian OpenStreetMap Team website for more information.

## Dataset Source

OpenStreetMap contributors

## Organization

Humanitarian OpenStreetMap Team (HOT)

## Dataset Date

[2023-03-04T00:00:00 TO 2023-03-04T23:59:59]

## Last Modified

2020-07-04T09:15:16

## Data Update Frequency

Every month

## Methodology

Other

## Methodology Other

Volunteered geographic information

## Caveats

OpenStreetMap data is crowd sourced and cannot be considered to be exhaustive

## Tags

geodata, roads, transportation