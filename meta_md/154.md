## Title

HOTOSM Brazil (North) Sea Ports (OpenStreetMap Export)

## Notes

OpenStreetMap exports for use in GIS applications. This theme includes all OpenStreetMap features in this area matching: amenity = 'ferry_terminal' OR building = 'ferry_terminal' OR port IS NOT NULL Features may have these attributes:  name addr:full source operator:type amenity addr:city building port  This dataset is one of many OpenStreetMap exports on HDX. See the Humanitarian OpenStreetMap Team website for more information.

## Dataset Source

OpenStreetMap contributors

## Organization

Humanitarian OpenStreetMap Team (HOT)

## Dataset Date

[2023-03-07T00:00:00 TO 2023-03-07T23:59:59]

## Last Modified

2020-06-10T22:01:52.425613

## Data Update Frequency

Every month

## Methodology

Other

## Methodology Other

Volunteered geographic information

## Caveats

OpenStreetMap data is crowd sourced and cannot be considered to be exhaustive

## Tags

facilities-infrastructure, geodata, logistics, ports, transportation