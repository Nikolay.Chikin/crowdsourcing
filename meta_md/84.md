## Title

Child Protection Area of Responsibility Organizations

## Notes

2022 HPC - Child protection organization response activities

## Dataset Source

Global CP AoR

## Organization

Global Child Protection AoR

## Dataset Date

[2022-06-10T00:00:00 TO 2022-12-10T23:59:59]

## Last Modified

2022-06-10T08:43:48.801648

## Data Update Frequency

Every six months

## Methodology

Registry

## Tags

humanitarian needs overview-hno, hxl, protection