## Title

Brazil - Environment

## Notes

Contains data from the World Bank's data portal. There is also a consolidated country dataset on HDX. Natural and man-made environmental resources – fresh water, clean air, forests, grasslands, marine resources, and agro-ecosystems – provide sustenance and a foundation for social and economic development.  The need to safeguard these resources crosses all borders.  Today, the World Bank is one of the key promoters and financiers of environmental upgrading in the developing world. Data here cover forests, biodiversity, emissions, and pollution. Other indicators relevant to the environment are found under data pages for Agriculture &amp; Rural Development, Energy &amp; Mining, Infrastructure, and Urban Development.

## Dataset Source

World Bank

## Organization

World Bank Group

## Dataset Date

[1960-01-01T00:00:00 TO 2022-12-31T23:59:59]

## Last Modified

2024-04-27T13:50:50.447281

## Data Update Frequency

Every month

## Methodology

Registry

## Caveats



## Tags

environment, hxl, indicators