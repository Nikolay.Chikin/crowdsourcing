## Title

HOTOSM Brazil (South1) Waterways (OpenStreetMap Export)

## Notes

OpenStreetMap exports for use in GIS applications. This theme includes all OpenStreetMap features in this area matching: waterway IS NOT NULL OR water IS NOT NULL OR natural IN ('water','wetland','bay') Features may have these attributes:  water depth waterway width tunnel blockage name covered layer source natural  This dataset is one of many OpenStreetMap exports on HDX. See the Humanitarian OpenStreetMap Team website for more information.

## Dataset Source

OpenStreetMap contributors

## Organization

Humanitarian OpenStreetMap Team (HOT)

## Dataset Date

[2021-02-19T00:00:00 TO 2021-02-19T23:59:59]

## Last Modified

2020-04-03T04:49:10

## Data Update Frequency

Every month

## Methodology

Other

## Methodology Other

Volunteered geographic information

## Caveats

OpenStreetMap data is crowd sourced and cannot be considered to be exhaustive

## Tags

geodata, hydrology, rivers