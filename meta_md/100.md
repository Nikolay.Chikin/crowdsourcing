## Title

Brazil: Hotels establishments in Ouro Preto

## Notes

Through the tripadvisor platform, information about hotels establishments  until 2021 for the city of Ouro Preto was obtained.

## Dataset Source

Instituto de Geografia – USFQ and U.S Department of State

## Organization

Hub Latin America

## Dataset Date

[2018-01-01T00:00:00 TO 2021-06-21T23:59:59]

## Last Modified

2021-10-10T05:03:54.642860

## Data Update Frequency

Every year

## Methodology

Registry

## Caveats

Created by TripAdvisor

## Tags

transportation