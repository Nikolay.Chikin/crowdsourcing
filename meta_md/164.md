## Title

HOTOSM Brazil (East) Health Facilities (OpenStreetMap Export)

## Notes

OpenStreetMap exports for use in GIS applications. This theme includes all OpenStreetMap features in this area matching: healthcare IS NOT NULL OR amenity IN ('doctors','dentist','clinic','hospital','pharmacy') Features may have these attributes:  addr:city addr:full healthcare:speciality amenity capacity:persons healthcare operator:type source name building  This dataset is one of many OpenStreetMap exports on HDX. See the Humanitarian OpenStreetMap Team website for more information.

## Dataset Source

OpenStreetMap contributors

## Organization

Humanitarian OpenStreetMap Team (HOT)

## Dataset Date

[2023-03-04T00:00:00 TO 2023-03-04T23:59:59]

## Last Modified

2020-06-03T22:47:36

## Data Update Frequency

Every month

## Methodology

Other

## Methodology Other

Volunteered geographic information

## Caveats

OpenStreetMap data is crowd sourced and cannot be considered to be exhaustive

## Tags

geodata, health, health facilities