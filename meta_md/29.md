## Title

Brazil - Education

## Notes

Contains data from the World Bank's data portal. There is also a consolidated country dataset on HDX. Education is one of the most powerful instruments for reducing poverty and inequality and lays a foundation for sustained economic growth. The World Bank compiles data on education inputs, participation, efficiency, and outcomes. Data on education are compiled by the United Nations Educational, Scientific, and Cultural Organization (UNESCO) Institute for Statistics from official responses to surveys and from reports provided by education authorities in each country.

## Dataset Source

World Bank

## Organization

World Bank Group

## Dataset Date

[1960-01-01T00:00:00 TO 2023-12-31T23:59:59]

## Last Modified

2024-04-27T13:50:38.519849

## Data Update Frequency

Every month

## Methodology

Registry

## Caveats



## Tags

education, hxl, indicators