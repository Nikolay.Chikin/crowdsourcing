## Title

Brazil - Subnational Administrative Boundaries

## Notes

This dataset contains the following administrative boundaries: ADM0, ADM1, ADM2.   Produced and maintained since 2017, the geoBoundaries Global Database of Political Administrative Boundaries Database www.geoboundaries.org is an open license, standardized resource of boundaries (i.e., state, county) for every country in the world.

## Dataset Source

Instituto Brasileiro de Geografia e Estatística (IBGE)Instituto Brasileiro de Geografia e Estatística (IBGE), OCHA ROLACOpenStreetMap, Wambacher

## Organization

geoBoundaries

## Dataset Date

[2017-01-01T00:00:00 TO 2020-12-31T23:59:59]

## Last Modified

2022-01-06T14:09:56.337288

## Data Update Frequency

Live

## Methodology

Other

## Methodology Other

All stages of data collection and processing are publicly available, as described in both [peer-reviewed academic outlets](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0231866) and in the public code base available through [GitHub](https://github.com/wmgeolab/geoBoundaries).

## Caveats

[Acknowledgement](https://www.geoboundaries.org/index.html#citation) of the geoBoundaries project is required for the use of this data.

## Tags

administrative boundaries-divisions, gazetteer, geodata