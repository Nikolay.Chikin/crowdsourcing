## Title

HOTOSM Brazil (East) Airports (OpenStreetMap Export)

## Notes

OpenStreetMap exports for use in GIS applications. This theme includes all OpenStreetMap features in this area matching: aeroway IS NOT NULL OR building = 'aerodrome' OR emergency:helipad IS NOT NULL OR emergency = 'landing_site' Features may have these attributes:  addr:city aeroway emergency:helipad addr:full capacity:persons operator:type source emergency name building  This dataset is one of many OpenStreetMap exports on HDX. See the Humanitarian OpenStreetMap Team website for more information.

## Dataset Source

OpenStreetMap contributors

## Organization

Humanitarian OpenStreetMap Team (HOT)

## Dataset Date

[2023-03-04T00:00:00 TO 2023-03-04T23:59:59]

## Last Modified

2020-06-03T22:47:36

## Data Update Frequency

Every month

## Methodology

Other

## Methodology Other

Volunteered geographic information

## Caveats

OpenStreetMap data is crowd sourced and cannot be considered to be exhaustive

## Tags

aviation, facilities-infrastructure, geodata, transportation