## Title

HOTOSM Brazil (East) Railways (OpenStreetMap Export)

## Notes

OpenStreetMap exports for use in GIS applications. This theme includes all OpenStreetMap features in this area matching: railway IN ('rail','station') Features may have these attributes:  layer addr:city addr:full railway operator:type ele name source  This dataset is one of many OpenStreetMap exports on HDX. See the Humanitarian OpenStreetMap Team website for more information.

## Dataset Source

OpenStreetMap contributors

## Organization

Humanitarian OpenStreetMap Team (HOT)

## Dataset Date

[2023-03-04T00:00:00 TO 2023-03-04T23:59:59]

## Last Modified

2020-07-02T13:50:30

## Data Update Frequency

Every month

## Methodology

Other

## Methodology Other

Volunteered geographic information

## Caveats

OpenStreetMap data is crowd sourced and cannot be considered to be exhaustive

## Tags

facilities-infrastructure, geodata, railways, transportation