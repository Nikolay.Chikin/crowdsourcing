## Title

Brazil - Protection Monitoring, 2020

## Notes

The size of the outflows from Venezuela sharply increased from some 700,000 in 2015 to over 4 million by June 2019, largely driven by a substantial deterioration of the situation in the country. Given the disruption of the functioning of some democratic institutions and rule of law, and its impact on the preservation of security, economic stability, health, public peace and the general welfare system, the crisis continues to worsen and serious human rights violations are widely reported. The displacement outside Venezuela has mostly affected countries in Latin America and the Caribbean, particularly Argentina, Brazil, Chile, Colombia, Ecuador, Peru, and the southern Caribbean islands. Most governments in the region have made efforts to facilitate access to territory, documentation and access to services, but the capacity of host countries has become overstretched to address the increasing protection and integration needs, resulting in tighter border controls being put in place. Protection monitoring is a core UNHCR activity which aims at ensuring an adequate and timely understanding of the protection situation of persons affected by forced displacement. The action-oriented nature of protection monitoring allows UNHCR to adapt to the needs and protection risks faced by persons displaced outside Venezuela and informs a broad range of responses.

## Dataset Source

UNHCR

## Organization

UNHCR - The UN Refugee Agency

## Dataset Date

[2019-05-01T00:00:00 TO 2020-04-30T23:59:59]

## Last Modified

2020-09-04T00:00:00

## Data Update Frequency

Never

## Methodology

Other

## Methodology Other

Unit of Analysis: Household and individual  
Sampling Procedure: UNHCR undertook a survey to understand the protection needs of Venezuelan persons of concern (asylum-seekers and refugees) by taking a sample of those who registered with UNHCR. This survey was conducted through the Protection Monitoring Tool (PMT).
		  
The survey was voluntary, and each respondent’s consent was obtained before posing questions. The responses are not a representative sampling of the Venezuelan population, but rather those who registered with UNHCR, and who consented to the survey. 

The survey was extensive, providing answers to over 100 questions. It covered general profiles, household composition, travel routes, education, and employment. The information was collected using KOBO Toolkit, the corporate tool of UNHCR.  
Data Collection Mode: Face-to-face [f2f]

## Caveats

The data must be requested via the resource download link

## Tags

education, health, livelihoods, protection