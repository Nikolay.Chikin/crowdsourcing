## Title

Brazil - Vulnerability Need Assessment (Boa Vista), 2019

## Notes

This one-off data collection exercise had the purpose of facilitating decision-making processes. The exercize took place within the month of July 2019 and no further frequency is expected. A total of 308 households were surveyed. The population currently living in shelters was classified in three categories for further intervention, namely:  1) Population willing to relocate and/or already registered in the Interiorization programme;  2) Population not willing to relocate to another part of the country;  3) Vulnerable population classified by evident Specific Needs.  These categories are not mutually exclusive.

## Dataset Source

UNHCR

## Organization

UNHCR - The UN Refugee Agency

## Dataset Date

[2019-07-01T00:00:00 TO 2019-07-30T23:59:59]

## Last Modified

2022-01-28T00:00:00

## Data Update Frequency

Never

## Methodology

Other

## Methodology Other

Kind of Data: Sample survey data [ssd]  
Unit of Analysis: Households living in shelters  
Sampling Procedure: Simple random sampling.  
Data Collection Mode: Face-to-face interview

## Caveats

The data must be requested via the resource download link

## Tags

livelihoods, needs assessment