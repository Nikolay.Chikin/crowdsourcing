## Title

Brazil - IDMC Event data (Internal Displacement Updates)

## Notes

Conflict and disaster population movement (flows) data for Brazil. The data is the most recent available and covers a 180 day time period.   Internally displaced persons are defined according to the 1998 Guiding Principles (https://www.internal-displacement.org/publications/ocha-guiding-principles-on-internal-displacement) as people or groups of people who have been forced or obliged to flee or to leave their homes or places of habitual residence, in particular as a result of armed conflict, or to avoid the effects of armed conflict, situations of generalized violence, violations of human rights, or natural or human-made disasters and who have not crossed an international border.   The IDMC's Event data, sourced from the Internal Displacement Updates (IDU), offers initial assessments of internal displacements reported within the last 180 days. This dataset provides provisional information that is continually updated on a daily basis, reflecting the availability of data on new displacements arising from conflicts and disasters. The finalized, carefully curated, and validated estimates are then made accessible through the Global Internal Displacement Database (GIDD), accessible at https://www.internal-displacement.org/database/displacement-data. The IDU dataset comprises preliminary estimates aggregated from various publishers or sources.

## Dataset Source

IDMC

## Organization

Internal Displacement Monitoring Centre (IDMC)

## Dataset Date

[2023-11-12T00:00:00 TO 2024-05-10T23:59:59]

## Last Modified

2024-05-10T19:49:26.053479

## Data Update Frequency

Every day

## Methodology

Other

## Methodology Other

IDMC defines an event as any distinct incident, whether prompted by natural hazards or conflict-related violence, resulting in the forced displacement of individuals within a specified geographical area confined to the borders of a single country. Whenever feasible, IDMC adopts the definitions of events provided by primary data sources, encompassing named disasters, government-recognized weather patterns, and conflict-related offensives, each delineated by their specific geographical and temporal parameters.

Internally displaced persons are "persons or groups of persons who have been forced or obliged to flee or to leave their homes or places of habitual residence, in particular as a result of or in order to avoid the effects of armed conflict, situations of generalized violence, violations of human rights or natural or human-made disasters, and who have not crossed an internationally recognized state border."

Internally displaced people are often confused with refugees. Unlike refugees, internally displaced people remain under the protection of their own government, even if their reason for fleeing was similar to that of refugees. Refugees are people who have crossed an international border to find sanctuary and have been granted refugee or refugee-like status or temporary protection.

## Caveats

Please note that most of the figures are estimates. The definition highlights two issues:

1) The coercive or otherwise involuntary character of movement. The definition mentions some of the most common causes of involuntary movements, such as armed conflict, violence, human rights violations and disasters. These causes have in common that they give no choice to people but to leave their homes and deprive them of the most essential protection mechanisms, such as community networks, access to services, livelihoods. Displacement severely affects the physical, socio-economic and legal safety of people and should be systematically regarded as an indicator of potential vulnerability.

2) The fact that such movement takes place within national borders. Unlike refugees, who have been deprived of the protection of their state of origin, IDPs remain legally under the protection of national authorities of their country of habitual residence. IDPs should therefore enjoy the same rights as the rest of the population. The Guiding Principles on Internal Displacement remind national authorities and other relevant actors of their responsibility to ensure that IDPs" rights are respected and fulfilled, despite the vulnerability generated by their displacement.

## Tags

climate hazards, conflict-violence, cyclones-hurricanes-typhoons, displacement, flooding-storm surge, hxl, internally displaced persons-idp, natural disasters